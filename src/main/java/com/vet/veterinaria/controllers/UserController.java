package com.vet.veterinaria.controllers;

import com.vet.veterinaria.dto.SimpleObjectResponse;
import com.vet.veterinaria.models.User;
import com.vet.veterinaria.services.impl.UserService;
import com.vet.veterinaria.utils.Constants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.mappers.ModelMapper;

import javax.validation.Valid;

@RestController
@Api(tags = "Account Type API")
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;

    }

    @ApiOperation(nickname = "create", notes = "Este método crea un usuario.", value = "Crear usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuario creado con éxito", response = User.class),
            @ApiResponse(code = 401, message = "Acceso no autorizado", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @PostMapping(value = "/create", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> create(@Valid @RequestBody User user) {
        userService.create(user);
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message(Constants.MESSAGEOK).value(null).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "create", notes = "Este método edita un usuario.", value = "Editar usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Editado con éxito", response = User.class),
            @ApiResponse(code = 401, message = "Acceso no autorizado", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @PutMapping(value = "/update", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> update(@Valid @RequestBody User user) {
        userService.create(user);
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message(Constants.MESSAGEOK).value(null).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "findall", notes = "Este método lista todos los usuarios.", value = "Listar Usuarios", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Listados con éxito", response = User.class),
            @ApiResponse(code = 401, message = "Acceso no autorizado", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @GetMapping(value = "/findUserAll", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findAll() {
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message(Constants.MESSAGEOK).value(userService.getAllUsers()).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "findbyid", notes = "Este método lista al usuario mediante la búsqueda con su id.", value = "Buscar Usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Encontrado con éxito", response = User.class),
            @ApiResponse(code = 401, message = "Acceso no autorizado", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @GetMapping(value = "/findById", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findById(@RequestParam Long id) {
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message(Constants.MESSAGEOK).value(userService.findById(id)).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "deletebyid", notes = "Este método elimina al usuario mediante su id.", value = "Eliminar Usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Eliminado con éxito", response = User.class),
            @ApiResponse(code = 401, message = "Acceso no autorizado", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @DeleteMapping(value = "/deleteById/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> deleteById(@PathVariable Long id) {
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message(Constants.MESSAGEOK).value(userService.deleteById(id)).build(),
                HttpStatus.OK);
    }


}
