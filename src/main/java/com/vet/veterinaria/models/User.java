package com.vet.veterinaria.models;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
// @Accessors(chain = true)
@Table(name= "tb_users")
@ApiModel(value = "Personal - Model", description = "Entidad para administrar la información del personal")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="us_id")
    private Long id;

    @Column(name="us_name")
    @NotNull(message = "us_name no puede ser nulo")
    @NotBlank(message = "us_name no puede ser vacia")
    @Size(max = 100)
    private String name;

    @Column(name="us_email")
    @NotNull(message = "us_email no puede ser nulo")
    @NotBlank(message = "us_email no puede ser vacia")
    @Size(max = 100)
    private String email;

    @Column(name="us_cellphone")
    @NotNull(message = "us_cellphone no puede ser nulo")
    @NotBlank(message = "us_cellphone no puede ser vacia")
    @Size(max = 10)
    private String cellphone;

    @Column(name="us_age")
    @NotNull(message = "us_age no puede ser nulo")
//    @NotBlank(message = "us_age no puede ser vacia")
//    @Size(max = 3)
    private int age;

    @Column(name="us_direction")
    @NotNull(message = "us_direction no puede ser nulo")
    @NotBlank(message = "us_direction no puede ser vacia")
    @Size(max = 100)
    private String direction;

}
