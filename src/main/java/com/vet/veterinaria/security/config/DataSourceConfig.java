package com.vet.veterinaria.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
@PropertySource(value = "classpath:/sql-${spring.profiles.active}.properties", ignoreResourceNotFound = true)
public class DataSourceConfig {

    @Bean(name = "DSBaseObject")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource primaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean("jdbcBaseObject")
    public JdbcTemplate createJdbcTemplateLogin(@Autowired @Qualifier("DSBaseObject") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}