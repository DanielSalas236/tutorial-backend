package com.vet.veterinaria.services.interfaces;

import java.util.List;
import java.util.Optional;

public interface IGenericService<T, I>{

        T save( T entity);
        T update( T entity);
        void delete( T entity);
        void deleteById( I id);
        Optional<T> findById(I id);
        List<T> findAll();
}
