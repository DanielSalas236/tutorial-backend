package com.vet.veterinaria.services.interfaces;

import com.vet.veterinaria.models.User;

import javax.validation.constraints.NotNull;

public interface IUserService extends IGenericService<User, Long> {



}
